
const URL = require('url');
const puppeteer = require('puppeteer');
const cheerio = require('cheerio');

const mandatoryFileds = ['url', 'codes', 'price', 'availability',];

let url = process.argv.pop();

if (url.length <= 0) {
  throw new Error('No url are present.');
}

try {
  url = URL.parse(url);
} catch (error) {
  console.log(error);
  throw new Error('A valid url is required.');
}

 (async(url) => {
  
  let parser;
  let source;
  let html;

  const filename = url.host.replace(/[^a-z0-9]/gi, '_').toLowerCase();
  
  try {    
    parser = require(`./parsers/${filename}`);
  } catch (error) {
    throw new Error(`Parser ${filename} not found.`);
  }
  
  try {
    console.log(`Loading page content fot the url ${url.href}`);
    const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
    // const browser = await puppeteer.launch({args: ['--no-sandbox']});
    const page = await browser.newPage();
    page.setViewport({height: 1920, width: 1080});
    await page.goto(url.href, {timeout: 30000 });
    source = await page.content();
    html = await cheerio.load(source);
  } catch (error) {
    console.error(error.message);
  }

  const isProductPage = await parser.isProductPage(url, html, source);

  if (!isProductPage) {
    console.log(`The url ${url.href} is not a valid product page.`);
    process.exit(1);
  }

  console.log('Valid product page.');

  const productData = await parser.getProductData(url, html, source);
  console.log(productData);

  for (let key of mandatoryFileds) {
    if (productData[key] === '' || productData[key] === undefined || productData[key] === null || productData[key].length <= 0) {      
      throw new Error(`Field ${key} is required`);
    }

    if (key === 'codes' && productData[key].length <= 0) {
      throw new Error(`Field ${key} is required`);
    }

    if (key === 'codes') {
      for (var codes of productData[key]) {    
        if (codes.code.length < 1) {
          throw new Error(`Code seems to be not valid`);
        }
    
        if (['ean', 'minsan', 'generic'].indexOf(codes.type) === -1) {
          throw new Error(`Code type seems to be not valid`);
        }
      }
    }

    
  }

  console.log();
  console.log('Product imported!');  


  
})(url);