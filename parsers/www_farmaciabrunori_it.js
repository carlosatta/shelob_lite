// Nicola Saba - nicola.sabaoz@gmail.com

module.exports = {

  // This function have to return true o false if the page is a valid product page or not
  async isProductPage(url, html, source) {
    if(url.pathname.includes('catalogo')){
    return true;
    }
  },


  async getProductData(url, html, source) {

    var codMinsan, codEan, codici = [];

    let title = html('h1').text();
        title = title.split('-');
        title = title[0];

    let code = html('div.item ul.lista').text().toLowerCase();
        code = code.replace(/scarica il coupon se vuoi acquistare questo prodotto in farmacia, allo stesso prezzo!/g, '');
        code = code.replace(/\n/g, ' ').replace(/                         /g, '').replace(/       /g, ' ').trim();
        
        if(code.includes('minsan:')){
          var minsan = (/minsan:([0-9]{9})/gmi).exec(code).pop();
          codMinsan = {
            code: minsan,
            type: 'minsan'
          }
          codici.push(codMinsan)
        }
        if(code.includes('ean:')){
          var ean = (/ean:([0-9]{13})/gmi).exec(code).pop();
          codEan = {
            code: ean,
            type: 'ean'
          }
          codici.push(codEan)
        }

    // html("*").contents().each(function() {
    //   if (this.nodeType === 8) {
    //     console.log(this.nodeValue);
    //   }
    // });

    let price = '';//html('div.price').text();    
    //     console.log('price:', price)
    //     price = price.replace('€', '').replace(',', '.');
    //     price = parseFloat(price);

    let availability = html('div.dispo span').attr('class');
        if(availability === 'red'){
          availability = 0;
        }else{availability = 1}

    let image = html('div.image.productImage img').attr('src');
        image = url.protocol+'//'+url.host+image;

    let category = null;

    return {
      title: title,
      url: url.href,
      codes: codici,
      image: image,
      price: price,
      category: category,
      availability: availability,
    };
  }


}
