// Manlio Diana manliodiana.md@gmail.com
module.exports = {
  // This function have to return true o false if the page is a valid product page or not
  async isProductPage(url, html, source) {
    let page = html('li.product').text().trim()
    if (page.length > 0) { 
      return true;
    }
  },
  
  async getProductData(url, html, source) {
    let minsan, ean, codeMinsan, codeEan, codici = [];

    let title = html('li.product').text().trim()

    let code = html('div.sku').text().toLowerCase().trim()
    code = code.replace(/\t/g, '').replace(/\n/g, ' ').replace(/  /g, ',')
    if ((code.includes('prodotto:')) || (code.includes('minsan:'))) { 
      minsan = (/minsan: ([0-9]{9})/gmi).exec(code).pop();
      codeMinsan = {
        code: minsan,
        type: 'minsan'
      };
      codici.push(codeMinsan)
    }

    let code2 = html('div#product_tabs_additional tr.odd').text().toLowerCase().trim()
    code2 = code2.replace(/ /g, '').replace(/\n/g, ' ')

    if (code2.includes('ean')) { 
      ean = (/ean ([0-9]{13})/gmi).exec(code2);
      if (ean != null) {
        ean = ean.pop()
        codeEan = {
          code: ean,
          type: 'ean'
        };
      codici.push(codeEan)        
      }
    }

    let price = html('div.product-shop p.special-price span.price').text().trim(); 
    if (price.length <= 0) {
      price = html('div.product-shop span.price').text().trim(); 
    } 

    price = price.replace('€', '').replace(',', '.');
    price = parseFloat(price);

    let availability = html('div.product-shop div.price-availability-block p.availability').attr('class');
    availability = availability.split(' ').pop()
    if ((availability == 'in-stock') || (availability == 'limitataDisponibilitaTop')) {
      availability = 1;
    } else {
      availability = 0;
    }

    let image = html('div.product-image img').attr('src');

    let category = null;

    return {
      title: title,
      url: url.href,
      codes: codici,
      image: image,
      price: price,
      category: category,
      availability: availability,
    };
  }
}

