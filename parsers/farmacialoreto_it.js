// Manlio Diana manliodiana.md@gmail.com
module.exports = {
  // This function have to return true o false if the page is a valid product page or not
  async isProductPage(url, html, source) {
    let page = html('div.wrapper-product-detail h1').text().trim();
    if (page.length > 0) { 
      return true;
    }
  },

  async getProductData(url, html, source) {
    let minsan, generic, ean, codeMinsan, codeGeneric, codeEan, codici = [];

    let title = html('div.wrapper-product-detail h1').text().trim();
    
    let code = html('div.block.description').text().trim();
    code = code.replace(/\t/g, '').replace(/  /g, '').replace(/\n/g, ' ');
    code = code.replace(/  /g, ' ').trim().toLowerCase();
    if ((code.includes('prodotto:')) || (code.includes('minsan:'))) { 
      minsan = (/prodotto: ([0-9]{9})/gmi).exec(code).pop();
      codeMinsan = {
        code: minsan,
        type: 'minsan'
      };
      codici.push(codeMinsan)
    }

    if (code.includes('ean')) { 
      ean = (/ean: ([0-9]{13})/gmi).exec(code).pop();
      codeEan = {
        code: ean,
        type: 'ean'
      };
      codici.push(codeEan)        
    }

    generic = html('div#tab-description').text().toLowerCase().trim();

    let deletes = html('div#tab-description div#tab-review').text().toLowerCase().trim();
    generic = generic.replace(deletes, '').trim();
    if (generic.includes('cod.')) { 
      generic = generic.split(' ').pop()
      codeGeneric = {
        code: generic,
        type: 'generic'
      };
      codici.push(codeGeneric)
    }

    let price = html('div.price span.price-new').text().trim();
    if (price.length <= 0) {
      price = html('div.price span').text().trim(); 
    }
    price = price.split('€').shift();
    price = price.replace(',', '.');
    price = parseFloat(price);

    let availability;
    code = code.split(':').pop().trim();
    if (code == 'in magazzino') {
      availability = 1 
    } else {
      availability = 0
    }

    let image = html('div.image a img').attr('src');

    let category = null;

    return {
      title: title,
      url: url.href,
      codes: codici,
      image: image,
      price: price,
      category: category,
      availability: availability,
    };
  }
}
