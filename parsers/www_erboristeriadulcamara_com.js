// Manlio Diana manliodiana.md@gmail.com
module.exports = {
  // This function have to return true o false if the page is a valid product page or not
  async isProductPage(url, html, source) {
    var sito = url.path
    if (sito.includes('id')) { 
      return true;
    }
  },

  async getProductData(url, html, source) {
    let title = html('table tbody tr td b font').text().trim();
    title = title.replace(/\|/g, '').trim().replace(/\t/g, '').replace(/  /g, ' ')
    title = title.split('\n');
    title = title[3]
    title = title.split('-').shift().trim();

    let price = html('u').text().trim(); 
    price = price.replace('€ ', '').replace(',', '.');
    price = price.split(' ')
    price = price.shift();
    price = parseFloat(price);
    let code = [];

    let availability = 1;

    let image = html('font img').attr('src');
    image = url.protocol + '//' + url.host + '/public/shopping/' + image

    let category = null;

    return {
      title: title,
      url: url.href,
      codes: code,
      image: image,
      price: price,
      category: category,
      availability: availability,
    };
  }
}