//falchilorenzo@gmail.com
module.exports = {


    // This function have to return true o false if the page is a valid product page or not
  async isProductPage(url, html, source) {
    var sito = (url.pathname).split('/');  
    if (sito.includes('p')){
      return true;
    }
  },
  
    async getProductData(url, html, source) {

      var info=[];

      let title = html('div h1.titoloArea').text();


      let code = html('div#contenitore_e_info_aggiuntive2 div.row22').text().trim().toLowerCase();
      code = code.replace(/\t/g, '').replace(/\n/g, ' ').replace(/  /g, ',');
      let codeMinsan;
      let codeEan;
      let Generic;

      if((code.includes('codice prodotto'))||code.includes('codice minsan')){
        var minsan = (/Codice prodotto ([0-9]{9})/gmi).exec(code).pop();
        codeMinsan = {
          code: minsan,
          type: 'minsan'
        }
        info.push(codeMinsan);
      };

      if(code.includes('codice ean')){
        var ean = (/Codice ean ([0-9]{13})/gmi).exec(code).pop();
        codeEan = {
          code: ean,
          type: 'ean'
        }
      }; 
      info.push(codeEan);

      let codeGeneric = html('div#descrizioneSchedaTop').text().trim();
      if(codeGeneric.includes('Cod.')){
      codeGeneric = codeGeneric.split(' ').pop();
      codeGeneric = codeGeneric.replace(/Cod./, '');
      codeGeneric = codeGeneric.replace(/\n/, '');
      Generic = {
        code: codeGeneric,
        type: 'generic'
      };
      info.push(Generic)
      } 
      
  
      let price = html('p.prezzo_finale2').text(); 
          price = price.replace('€', '').replace(',', '.');
          price = parseFloat(price);
            
      let availability = html('div.testoDisponibilita').attr('class');
          availability = availability.split(' ').pop();
        if((availability === 'altaDisponibilitaTop') || (availability === 'limitataDisponibilitaTop')){
            availability =  1;
        } else if (availability === 'noDisponibilitaTop') {
            availability =  0;
        };
  
      let image = html('img.img-responsive').attr('src');
      image = url.protocol + '//' + url.host + image;

      let category = null;
  

      return {
        title: title,
        url: url.href,
        codes: info,
        image: image,
        price: price,
        category: category,
        availability: availability,
      };
    }
  
  
    }