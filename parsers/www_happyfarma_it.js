// Manlio Diana manliodiana.md@gmail.com
module.exports = {
  // This function have to return true o false if the page is a valid product page or not
  async isProductPage(url, html, source) {
    let page = html('div.product-shop h1.h1').text().trim()
    if (page.length > 0) { 
      return true;
    }
  },

  async getProductData(url, html, source) {
    let minsan, generic, codeMinsan, codeGeneric, codici = [];

    let title = html('div.product-shop h1.h1').text().trim()

    let code = html('div.sku').text().toLowerCase().trim()
    code = code.replace(/\t/g, '').replace(/\n/g, ' ').replace(/  /g, ',')
    if ((code.includes('prodotto:')) || (code.includes('minsan:'))) { 
      minsan = (/minsan: ([0-9]{9})/gmi).exec(code).pop();
      codeMinsan = {
        code: minsan,
        type: 'minsan'
      };
      codici.push(codeMinsan)
    }
    
    generic = html('div.std').text().toLowerCase().trim()
    if (generic.includes('cod.')) { 
      generic = generic.split(' ').pop()
      generic = generic.replace('cod.', '')
      codeGeneric = {
        code: generic,
        type: 'generic'
      };
      codici.push(codeGeneric)
    }

    let price = html('div.product-shop p.special-price span.price').text().trim(); 
    if (price.length <= 0) {
      price = html('div.product-shop span.price').text().trim(); 
    } 

    price = price.replace('€', '').replace(',', '.');
    price = parseFloat(price);

    let availability = html('div.extra-info p.availability').attr('class');
    availability = availability.split(' ').pop();
    if ((availability == 'in-stock') || (availability == 'limitataDisponibilitaTop')) {
      availability = 1;
    } else {
      availability = 0;
    }

    let image = html('img#image-main').attr('src');

    let category = null;

    return {
      title: title,
      url: url.href,
      codes: codici,
      image: image,
      price: price,
      category: category,
      availability: availability,
    };
  }
}