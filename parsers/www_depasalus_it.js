// Manlio Diana manliodiana.md@gmail.com
module.exports = {
  // This function have to return true o false if the page is a valid product page or not
  async isProductPage(url, html, source) {
    let page = url.href.split('.').pop();
    if (page == 'aspx#!/') { 
      return true;
    }
  },
  
  async getProductData(url, html, source) {
    let generic, codeGeneric, codici = [];

    let title = html('h1').text().trim()

    generic = html('div#tab1 span.clearfix div').text().toLowerCase().trim()
    if (generic.includes('cod.')) { 
      generic = generic.split(' ').pop()
      codeGeneric = {
        code: generic,
        type: 'generic'
      };
      codici.push(codeGeneric)
    }

    let price = html('div.ecommscheda-price-zone div div span.ecommscheda-boxprice').text().trim(); 
    price = price.split('€');
    price.pop();
    price = price.pop().trim();
    price = price.replace(',', '.');
    price = parseFloat(price);

    let availability = 1;

    let image = html('span.ecommscheda-boximg img').attr('src');
    image = url.protocol + '//' + url.host + image

    let category = null;

    return {
      title: title,
      url: url.href,
      codes: codici,
      image: image,
      price: price,
      category: category,
      availability: availability,
    };
  }
}

