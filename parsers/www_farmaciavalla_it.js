//falchilorenzo@gmail.com

module.exports = {


    // This function have to return true o false if the page is a valid product page or not
  async isProductPage(url, html, source) {
    url = url.pathname;
    if(url.includes('articolo')){
        return true;
    }
  },
  
    async getProductData(url, html, source) {

      var info=[];

      let title = html('div.right div.heading-product h1').text();

      let Generic;
      let codeGeneric = html('div#tab-description').text();
      codeGeneric = codeGeneric.split(' ');
      codeGeneric = codeGeneric[codeGeneric.length-12];
      if(codeGeneric != undefined){
        Generic = {
            code: codeGeneric.replace(/\n/,''),
            type: 'generic'
      };
        info.push(Generic)
    }

      
  
      let price = html('div.price-container span.price-new').text();
      price = price.replace('€', '').replace(',', '.');
      price = parseFloat(price);
            
      let availability = html('div.description').text().trim().split(' ');
      if ((availability.includes('Non')) || (availability.includes('Disponibile'))){
          availability = 0;
      } else if ((availability.includes('Disponibile')) || (availability.includes('Subito'))){
          availability = 1;
      }
  
      let image = html('div.product-info div.left div.image img').attr('src');
      image = url.protocol + '/' + image;
      

      let category = null;
  

      return {
        title: title,
        url: url.href,
        codes: info,
        image: image,
        price: price,
        category: category,
        availability: availability,
      };
    }
  
  
    }