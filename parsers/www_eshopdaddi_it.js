// Manlio Diana manliodiana.md@gmail.com
module.exports = {
  // This function have to return true o false if the page is a valid product page or not
  async isProductPage(url, html, source) {
    var sito = [];

    sito = url.pathname.split('/')
    if (sito.includes('p')) { 
      return true;
    }
  },

  async getProductData(url, html, source) {
    let minsan, ean, generic, codeMinsan, codeEan, codeGeneric, codici = [];

    let title = html('div h1').text()

    let code = html('div#contenitore_e_info_aggiuntive2 div.row22 ').text().toLowerCase().trim()
    code = code.replace(/\t/g, '').replace(/\n/g, ' ').replace(/  /g, ',')
    if ((code.includes('codice prodotto')) || (code.includes('codice minsan'))) { 
      minsan = (/Codice prodotto ([0-9]{9})/gmi).exec(code).pop();
      codeMinsan = {
        code: minsan,
        type: 'minsan'
      };
      codici.push(codeMinsan)
    }

    if (code.includes('codice ean')) { 
      ean = (/Codice ean ([0-9]{13})/gmi).exec(code).pop();
      codeEan = {
        code: ean,
        type: 'ean'
      };
      codici.push(codeEan)

    }

    generic = html('div#descrizioneSchedaTop').text().toLowerCase().trim()
    if (generic.includes('cod.')) { 
      generic = generic.split(' ').pop()
      generic = generic.replace('cod.', '')
      codeGeneric = {
        code: generic,
        type: 'generic'
      };
      codici.push(codeGeneric)
    }

    let price = html('div p.prezzo_finale2').text(); 
    price = price.replace('€', '').replace(',', '.');
    price = parseFloat(price);

    let availability = html('div.testoDisponibilita').attr('class');
    availability = availability.split(' ').pop()
    if ((availability == 'altaDisponibilitaTop') || (availability == 'limitataDisponibilitaTop')) {
      availability = 1;
    } else {
      availability = 0;
    }

    let image = html('div.foto a.thumbnail').attr('href');
    image = url.protocol + '//' + url.host + image

    let category = null;

    return {
      title: title,
      url: url.href,
      codes: codici,
      image: image,
      price: price,
      category: category,
      availability: availability,
    };
  } 
}
