// Nicola Saba nicola.sabaoz@gmail.com

module.exports = {

  // This function have to return true o false if the page is a valid product page or not
  async isProductPage(url, html, source) {
    if(source.includes('id="product"')){
        return true
    }else{return false}       
  },

  async getProductData(url, html, source) {
    
    let title = html('div#fs_product_intro h1').text();

    let code

    let price = html('span#our_price_display').text();
        price = price.replace('€', '').replace(',', '.');
        price = parseFloat(price);
        

    let availability = html('p#pQuantityAvailable').text().trim();
        availability = availability.replace(/\n/g, '').replace(/\t/g, '').replace(/pezzi/g, '').replace(/pezzo/g, '');
        availability = availability.split(' ');
        availability = availability[0];
        if(availability === 'Disponibile'){
            availability = 1;
        }
        if(availability === ''){
            availability = 0
        }
    

    let image = html('img#bigpic').attr('src');
    
    
    let category = null;

    return {
      title: title,
      url: url.href,
      codes: code,
      image: image,
      price: price,
      category: category,
      availability: availability,
    };
  }


}