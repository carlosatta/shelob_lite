// Nicola Saba nicola.sabaoz@gmail.com

module.exports = {

  // This function have to return true o false if the page is a valid product page or not
  
  async isProductPage(url, html, source) {
    
    
    if(html('div.hdisp h2.hd').text().toLowerCase().includes('disponibilità')){
      return true;
    }else{return false}    
  },


  async getProductData(url, html, source) {

    var codMinsan, codEan, codGeneric, codici = [];
    
    let title = html('div.spacerPad.first h1.kheader').text().toLowerCase();
        title = title.replace(/modalità di pagamento/g, '').trim();

    let code = html('div.kinfo').text().toLowerCase().trim();
        // code = code.replace(/\n/g, '');
        console.log('code:', code)
    // if(code.includes('codice ministeriale:')){
    //   var minsan = (/codiceministeriale:&nbsp;([0-9]{9})/gmi).exec(code);
    //   codMinsan = {
    //     code: minsan,
    //     type: 'minsan'
    //   }
    //   codici.push(codMinsan)
    // }

    // if(code.includes('ean:')){
    //   var ean = (/ean:([0-9]{13})/gmi).exec(code).pop();
    //   codEan = {
    //     code: ean,
    //     type: 'ean'
    //   }
    //   codici.push(codEan)
    // }

    // var generic = html('div.description-section div div').text().toLowerCase().trim();
    
    // if(generic.includes('cod.')){
    //   generic = generic.split('cod.').pop();
    //   codGeneric = {
    //     code: generic,
    //     type: 'generic'
    //   }
    //   codici.push(codGeneric)
    // }
        
        

    let price = html('div.price span').text();
        price = price.replace('€', '').replace(',', '.');
        price = parseFloat(price);
        

    let availability = html('div.hdisp h2.hd span.dspKK').text();
    if(availability.includes('Ultimi')){
        availability = availability.split(' ');
        availability = parseFloat(availability[1]);
    }
    if(availability === 'Immediata'){
        availability = 1;
    }
    console.log('availability:', availability)

    let image = html('img.product-image').attr('src');
    
    
    let category = null;

    return {
      title: title,
      url: url.href,
      codes: codici,
      image: image,
      price: price,
      category: category,
      availability: availability,
    };
  }


}