// Manlio Diana manliodiana.md@gmail.com
module.exports = {
  // This function have to return true o false if the page is a valid product page or not
  async isProductPage(url, html, source) {
  let page = html('h1.nomeprodotto').text().trim();
    if (page.length > 0) { 
      return true;
    }
  },

  async getProductData(url, html, source) {
    let minsan, ean, generic, codeMinsan, codeEan, codeGeneric, codici = [];

    let title = html('h1.nomeprodotto').text().trim();

    let code = html('div.product-info p').text().toLowerCase().trim();
    code = code.replace(/\t/g, '').replace(/\n/g, ' ').replace(/  /g, ',')
    if ((code.includes('prodotto:')) || (code.includes('minsan:'))) { 
      minsan = (/prodotto: ([0-9]{9})/gmi).exec(code).pop();
      codeMinsan = {
        code: minsan,
        type: 'minsan'
      };
      codici.push(codeMinsan);
    }

    if (code.includes('ean:')) { 
      ean = (/ean: ([0-9]{13})/gmi).exec(code).pop();
      codeEan = {
        code: ean,
        type: 'ean'
      };
      codici.push(codeEan);
    }

    
    generic = html('div#tab-1 div').text().toLowerCase().trim();
    if (generic.includes('cod.')) { 
      generic = generic.split(' ').pop();
      generic = generic.replace('cod.', '');
      codeGeneric = {
        code: generic,
        type: 'generic'
      };
      codici.push(codeGeneric)
    }

    let price = html('div.product-price span.price').text().trim();
    price = price.split('€').pop();
    price = price.replace(',', '.');
    price = parseFloat(price);

    let availability = html('div.product-info p').text().trim().split(':');
    availability = availability[2]
    availability = availability.split('C').shift().toLowerCase();
    if (availability == 'non disponibile') {
      availability = 0
    } else {
      availability = 1
    }

    let image = html('div.primary-image a img').attr('src');

    let category = null;

    return {
      title: title,
      url: url.href,
      codes: codici,
      image: image,
      price: price,
      category: category,
      availability: availability,
    };
  }
}