// Manlio Diana manliodiana.md@gmail.com
module.exports = {
  // This function have to return true o false if the page is a valid product page or not
  async isProductPage(url, html, source) {
  let page = html('div.product-name').text().trim()
    if (page.length > 0) { 
      return true;
    }
  },

  async getProductData(url, html, source) {
    let title = html('div.product-name').text().trim();

    let code = [];

    let price = html('div.product-price span').text().trim();
    price = price.replace('€', '').replace(',', '.');
    price = parseFloat(price);

    let availability = 1 

    let image = html('div img').attr('src');

    let category = null;

    return {
      title: title,
      url: url.href,
      codes: code,
      image: image,
      price: price,
      category: category,
      availability: availability,
    };
  }
}