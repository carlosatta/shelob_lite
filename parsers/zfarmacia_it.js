// Nicola Saba nicola.sabaoz@gmail.com

module.exports = {

  // This function have to return true o false if the page is a valid product page or not
  async isProductPage(url, html, source) {
    
    if(url.pathname.includes('prodotti') || url.pathname.includes('farmaci') && html('div h1')!=0){      
      return true;
    }else{return false}    
  },


  async getProductData(url, html, source) {

    var codMinsan, codEan, codGeneric, codici = [];
    
    let title = html('div h1').text();

    let code = html('div.subtitle').text().toLowerCase();
        code = code.replace(/ /g, '');
    if(code.includes('codice:')){
      var minsan = (/codice:([0-9]{9})/gmi).exec(code).pop();
      codMinsan = {
        code: minsan,
        type: 'minsan'
      }
      codici.push(codMinsan)
    }

    if(code.includes('ean:')){
      var ean = (/ean:([0-9]{13})/gmi).exec(code).pop();
      codEan = {
        code: ean,
        type: 'ean'
      }
      codici.push(codEan)
    }

    var generic = html('div.description-section div div').text().toLowerCase().trim();
    
    if(generic.includes('cod.')){
      generic = generic.split('cod.').pop();
      codGeneric = {
        code: generic,
        type: 'generic'
      }
      codici.push(codGeneric)
    }
        
        

    let price = html('div.price span').text();
        price = price.replace('€', '').replace(',', '.');
        price = parseFloat(price);
        

    let availability = html('div.product-info div.availability').text().toLowerCase().trim();
    if(availability != 'non disponibile'){
      availability = 1;
    }else{availability = 0}
    

    let image = html('img.product-image').attr('src');
    
    
    let category = null;

    return {
      title: title,
      url: url.href,
      codes: codici,
      image: image,
      price: price,
      category: category,
      availability: availability,
    };
  }


}