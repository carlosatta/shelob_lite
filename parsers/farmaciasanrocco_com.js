//falchilorenzo@gmail.com

module.exports = {


    // This function have to return true o false if the page is a valid product page or not
  async isProductPage(url, html, source) {
    var prod = html('dl.dl-horizontal dt').text().toLowerCase().split(':');
    if(prod.includes('marca')){
        return true;
    }
  },
  
    async getProductData(url, html, source) {

      var info=[];

      let title = html('h1').text();

      var minsan = html('dd#myModel').text();
      let codeMinsan;
      let Generic;
        codeMinsan = {
          code: minsan,
          type: 'minsan'
        }
        info.push(codeMinsan);

      let codeGeneric = html('div#ProductDescriptionSystem_Bx7ALQdc').text();
      codeGeneric = codeGeneric.split(' ');
      codeGeneric = codeGeneric.pop();
      if (codeGeneric != '\n\n\n'){
      Generic = {
        code: codeGeneric,
        type: 'generic'
      };
      info.push(Generic)
    }
      
  
      let price;
      var prezzo = html('span.tb_integer').text().trim().split(' ');
      prezzo = prezzo[0];
      var virgola = html('div.price span.tb_decimal_point').text();
      virgola = virgola[0];
      var decimale = html('div.price span').attr('class');
      if(decimale === 'price-new'){
        decimale = html('div.price span.price-new span.tb_decimal').text();
      } else if(decimale === 'price-regular'){
        decimale = html('div.price span.price-regular span.tb_decimal').text();
      }
      price = prezzo + virgola + decimale;
      price = price.replace('€', '').replace(',', '.');
      price = parseFloat(price);



            
      let availability = html('dd#stock-text').text().toLowerCase();
      if(availability === 'disponibile'){
          availability =  1;
      } else {
          availability =  0;
      };
  
      let image = html('div.mSCover').attr('style');
      if(image != undefined){
        image = image.split("'")
        image = image[1];
      } else {
        image = '';
      };
      

      let category = null;
  

      return {
        title: title,
        url: url.href,
        codes: info,
        image: image,
        price: price,
        category: category,
        availability: availability,
      };
    }
  
  
    }