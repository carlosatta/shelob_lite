// Nicola Saba nicola.sabaoz@gmail.com

module.exports = {

  // This function have to return true o false if the page is a valid product page or not
  async isProductPage(url, html, source) {
    
    var urlPath = url.pathname.split("/");
    if(urlPath.includes('p')){      
      return true;
    }else{return false}    
  },


  async getProductData(url, html, source) {

    var codMinsan, codEan, codGeneric, codici = [];
    
    let title = html('h1').text()    

    let code = html('div.contenitore_e_info_aggiuntive2 div.row22.dicitura2').text().toLowerCase();
        code = code.replace(/\n/g, ' ').replace(/\t/g, '');
    if(code.includes('codice prodotto')){
      var minsan = (/Codice prodotto ([0-9]{9})/gmi).exec(code).pop();
      codMinsan = {
        code: minsan,
        type: 'minsan'
      }
      codici.push(codMinsan)
    }

    if(code.includes('codice ean')){
      var ean = (/Codice ean ([0-9]{13})/gmi).exec(code).pop();
      codEan = {
        code: ean,
        type: 'ean'
      }
      codici.push(codEan)
    }

    var generic = html('div#descrizioneSchedaTop').text().toLowerCase().trim();
    
    if(generic.includes('cod.')){
      generic = generic.split('cod.').pop();
      codGeneric = {
        code: generic,
        type: 'generic'
      }
      codici.push(codGeneric)
    }
        
        

    let price = html('p.prezzo_finale2').text();
        price = price.replace('€', '').replace(',', '.');
        price = parseFloat(price);
        

    let availability = 1;
    

    let image = html('a.thumbnail').attr('href');
        image = url.protocol+'//'+url.host+image;
    
    
    let category = null;

    return {
      title: title,
      url: url.href,
      codes: codici,
      image: image,
      price: price,
      category: category,
      availability: availability,
    };
  }


}