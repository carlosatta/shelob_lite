module.exports = {

  // This function have to return true o false if the page is a valid product page or not
  async isProductPage(url, html, source) {
    return true;
  },


  async getProductData(url, html, source) {
    let title = html('div.prod_page h2.product-name').text()

    let code = html('div.prod_page div.description').text();
        code = (/Codice prodotto: ([0-9]{9})/gmi).exec(code).pop();

    let price = html('div.product-info span#price-special').text() || html('div.product-info span.price-old').text(); 
        price = price.replace('€', '').replace(',', '.');
        price = parseFloat(price);

    let availability = html('div.prod_page div.description').text();
      try {
        availability = (/Disponibilità: (Disponibile)/gmi).exec(availability).pop();
        availability = !!availability ? 1 : 0
      } catch (error) {
        availability = 0;
      }

    let image = html('div.prod_page div.product-image img').attr('data-zoom-image');
    let category = null;

    return {
      title: title,
      url: url.href,
      codes: [{
        code: code,
        type: 'minsan'
      }],
      image: image,
      price: price,
      category: category,
      availability: availability,
    };
  }


}
